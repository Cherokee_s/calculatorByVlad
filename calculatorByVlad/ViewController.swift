//
//  ViewController.swift
//  calculatorByVlad
//
//  Created by Влад Серебряков on 10.02.2023.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: - Properties
    
    private var first = ""
    private var second = ""
    
    private var result = 0.0
    private var resultIsInt: Bool {
            return floor(result) == result
        }
    private var function: FunctionType = .undefined {
        didSet {
            pickFunctionType()
        }
    }
    private var input = ""
    
    private var selectedButton: CalculatorButton?
    
    // MARK: - IBOutlets
        
    @IBOutlet weak var calculatorDisplay: UILabel!
    @IBOutlet weak var buttonsView: ButtonsView!
    
    @IBOutlet weak var divideButton: UIButton!
    @IBOutlet weak var multiplyButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    
    // MARK: - Lifecycle
            
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonsView.delegate = self
    }
    
    // MARK: - IBActions
    
    @IBAction func clear(_ sender: Any) {
        makeButtonDeselect()
        
        first = ""
        second = ""
        function = .undefined
        result = 0.0
        input = ""
        calculatorDisplay.text = "0"
    }
    
    @IBAction func divide(_ sender: Any) {
        makeButtonDeselect(sender as? CalculatorButton)
        makeButtonSelected(sender as? CalculatorButton)
        
        function = .divide
        first = input
        input = ""
    }
    
    @IBAction func multiply(_ sender: Any) {
        makeButtonDeselect(sender as? CalculatorButton)
        makeButtonSelected(sender as? CalculatorButton)
        
        function = .multiply
        first = input
        input = ""
    }
    
    @IBAction func minus(_ sender: Any) {
        makeButtonDeselect(sender as? CalculatorButton)
        makeButtonSelected(sender as? CalculatorButton)
        
        function = .minus
        first = input
        input = ""
    }
    
    @IBAction func plus(_ sender: Any) {
        makeButtonDeselect(sender as? CalculatorButton)
        makeButtonSelected(sender as? CalculatorButton)
        
        function = .plus
        first = input
        input = ""
    }
    
    @IBAction func percent(_ sender: Any) {
        if first == "" {
            first = input
        } else {
            second = input
        }
        
        let firstDouble = Double(first) ?? 0.0
        let secondDouble = Double(second) ?? 0.0
        
        if second == "" {
            result = firstDouble / 100
        } else {
            result = (firstDouble * secondDouble) / 100
        }
        input = String(result)
        calculatorDisplay.text = String(result)
    }
    
    @IBAction func negate(_ sender: Any) {
        var inputDouble = Double(input) ?? 0.0
        inputDouble.negate()
        input = String(inputDouble)
        result = inputDouble
        calculatorDisplay.text = resultIsInt ? String(Int(result)) : String(result)
    }
    
    
    @IBAction func equal(_ sender: Any) {
        makeButtonDeselect()
        
        second = input
        
        let firstDouble = Double(first) ?? 0.0
        let secondDouble = Double(second) ?? 0.0
        
        switch function {
        case .plus:
            result = firstDouble + secondDouble
            calculatorDisplay.text = resultIsInt ? String(Int(result)) : String(result)
        case .minus:
            result = firstDouble - secondDouble
            calculatorDisplay.text = resultIsInt ? String(Int(result)) : String(result)
        case .multiply:
            result = firstDouble * secondDouble
            calculatorDisplay.text = resultIsInt ? String(Int(result)) : String(result)
        case .divide:
            result = firstDouble / secondDouble
            calculatorDisplay.text = resultIsInt ? String(Int(result)) : String(result)
        case .undefined:
            break
        }
        function = .undefined
        
    }
    
    @IBAction func decimal(_ sender: Any) {
        guard let containDot = calculatorDisplay.text?.contains("."),
              !containDot
        else { return }
        
        calculatorDisplay.text = ""
        input += "."
        calculatorDisplay.text! += input

    }
    
    @IBAction func number(_ sender: Any) {
        makeButtonDeselect(sender as? CalculatorButton)
        
        let calculatorButton = sender as! CalculatorButton
        
        calculatorDisplay.text = ""
        input += calculatorButton.getButtonValue()
        calculatorDisplay.text! += input
    }
    
    // MARK: - Utils
    
    func makeButtonSelected(_ newButton: CalculatorButton?) {
        if let newbutton = newButton {
            newbutton.isSelected = true
            selectedButton = newbutton
        }
    }
    
    func makeButtonDeselect(_ newButton: CalculatorButton? = nil) {
        if let newbutton = newButton, newbutton == selectedButton { return }
        if let oldbutton = selectedButton {
            oldbutton.forceToDeselect()
            selectedButton = nil
        }
    }
    
    private func pickFunctionType() {
        switch function {
            
        case .plus:
            
            plusButton.backgroundColor = .white
            plusButton.tintColor = .systemOrange
            minusButton.backgroundColor = .systemOrange
            multiplyButton.backgroundColor = .systemOrange
            divideButton.backgroundColor = .systemOrange
        case .minus:
            
            minusButton.backgroundColor = .white
            minusButton.tintColor = .systemOrange
            plusButton.backgroundColor = .systemOrange
            multiplyButton.backgroundColor = .systemOrange
            divideButton.backgroundColor = .systemOrange
            
        case .multiply:
            
            multiplyButton.backgroundColor = .white
            multiplyButton.tintColor = .systemOrange
            plusButton.backgroundColor = .systemOrange
            minusButton.backgroundColor = .systemOrange
            divideButton.backgroundColor = .systemOrange
            
        case .divide:
            
            divideButton.backgroundColor = .white
            divideButton.tintColor = .systemOrange
            plusButton.backgroundColor = .systemOrange
            minusButton.backgroundColor = .systemOrange
            multiplyButton.backgroundColor = .systemOrange
            
        case .undefined:
            
            plusButton.backgroundColor = .systemOrange
            plusButton.tintColor = .white
            
            minusButton.backgroundColor = .systemOrange
            minusButton.tintColor = .white

            multiplyButton.backgroundColor = .systemOrange
            multiplyButton.tintColor = .white

            divideButton.backgroundColor = .systemOrange
           divideButton.tintColor = .white

        }
    }
}

// MARK: - ButtonsViewDelegate

extension ViewController: ButtonsViewDelegate {
    func sendSelectedButton(_ button: CalculatorButton) {
        if ["+", "−", "÷", "×"].contains(button.getButtonValue()) {
            selectedButton = button
        }
    }
}

// MARK: - Enums

enum FunctionType {
    case plus
    case minus
    case multiply
    case divide
    
    case undefined
}
